// MongdoDB Aggregation

/*
    - To generate and perform operations 
    to create filtered results
    that helps us analyze the data.

 */

    // using aggrgate method:
     /*
        $match is used to pass the 
        documents that meet the specified condition
        to the next stage or aggregation process

        syntax:
        {$match:{field: value}}

     */

        db.fruits.aggregate(
            [
                {$match: {onSale: true}}
            ]
        )

    /*

            - The $group is used to group the elements together and
            field-value the date from the grouped element

            syntax:
            {$group: _id: "fieldSetGroup",}
     */

            db.fruits.aggregate(
                [
                    {$match: {onSale: true}},
                    {$group: {_id:"$supplier_id", totalFruits:{$sum:"$stock"}}},
                    {$project: {_id:0}}
                ]
            )


            db.fruits.aggregate(
                [
                    {$match: {onSale: true}},
                    {$group: {_id:"$supplier_id", totalFruits:{$sum:"$stock"}}},
                    {$sort: {totalFruits:-1}} // sort decrement if -1, if 1 sort increment
                ]
            )

            db.fruits.aggregate(
                [
                    {$match:{color:'Yellow'}},
                    {$group: {_id: "$suplier_id", stocks: {$sum:"$stock"}}}
                ]
            )



//aggregating results based on an array fields

/*
    the $unwind deconstructs an
    array field from a collection/field with an array value to output a result
 */

    db.fruits.aggregate(
        [
            {$unwind: "$origin"},
            {$group: {_id: "$origin", fruits:{$sum:1}}} // use $sum: 1 to count
        ]
    )


    /*
        Other aggregate stages
        $count all yellow fruits
        
     */

        db.fruits.aggregate(
            [
                {$match: {color:'Yellow'}},
                {$count:'Yellow Fruits'} // count the number of fruits with yellow color then add the field "Yellow Fruiets"
            ]
        )



        //$avg get the average value of the stock

        db.fruits.aggregate(
            [
                {$match:{color:'Yellow'}},
                {$group:{_id:"$color", avgYellow:{$avg: "$stock"} }}
            ]
        )


        /*
            $min and $max
        */

            db.fruits.aggregate(
                [
                    {$match:{color:'Yellow'}},
                    {$group:{_id:"$color", minYellow:{$min: "$stock"} }}
                ]
            )

            db.fruits.aggregate(
                [
                    {$match:{color:'Yellow'}},
                    {$group:{_id:"$color", maxYellow:{$max: "$stock"} }}
                ]
            )